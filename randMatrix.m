%(0.5) Desarrollar un subprograma function que genere una matriz de n filas y m columnas, utilizando la función random, con valores entre 0 y 25.
%Se declara la función
function matrix=randMatrix(filas,columnas)
  %De define el rango por defecto
  max=25;
  min=0;
  %Se genera una matriz de numero aleatorios en el rango dado.
  matrix=randi([min max],filas,columnas);
end