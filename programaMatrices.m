%    3. (1) Desarrolle un programa en un script, el cual deberá 
%        a. Generar una matriz con el function del ejercicio 1, e imprimirla
%        b. determinar si la matriz anterior (generada en a) ) es cuadrada y si lo es, utilizando el function del ejercicio 2  determine si la matriz es mágica.  
%        c. Genere una matriz mágica con magic, y pruebe con la function del ejercicio 2 si es mágica,
%        d. Imprima resultados y mensajes que corresponda.


% a. Generar una matriz
fprintf('Bienvenido ..... \n');
fprintf('generar matriz aleatoria:\n');
tamanho = input('Ingrese las dimensiones de la matriz de la forma [filas columnas]: ');
filas=tamanho(1);
columnas=tamanho(2);
matriz=randMatrix(filas, columnas);
fprintf('La matriz aleatoria generada es: \n');
disp(matriz);
%b.
[nFilas nColumnas]=size(matriz);
  if(nFilas==nColumnas)
    if(isMagic(matriz))
      fprintf('La matriz es magica \n');
    else
      fprintf('La matriz no es magica \n');
    end
  else
    fprintf('no es magica: las dimensiones no son iguales\n');
  end
%c. 
%dim opcional, puede declararlo directamente en magic
dim=input('ingrese un número entero para generar una matriz magica:')
 fprintf('generando una matriz magica... \n');
 magica=magic(dim)
 if(isMagic(magica)==true)
      fprintf('La matriz es magica \n');
  else
      fprintf('La matriz no es magica \n');
end
 


