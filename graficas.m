%    5. (2) Desarrolle un programa para dibujar en una sola ventana
% en tres gráficas distintas las funciones 
%        a. y = 2x0.3 + x3-0.23 - 2 
%        b. w = y2x - cos(y) + 1 
%        c. q = sen(x) + 3x 

%con x variando en el intervalo de -3 a 50 a intervalos de 0.03. 
%La primera en líneas amarilla con estilo de línea punto,
% la segunda en líneas verde discontinua y los puntos con el símbolo cruz, 
%en el caso de la tercera sea creativo en cuanto a tipo de líneas, color y puntos.
% Debe incluir una cuarta gráfica donde se graficará todas las funciones en 
%conjunto en escala logarítmica, además de proporcionar una leyenda, título y
% nombre en los ejes. 
x=-3:0.03:50;

y=2*x.^(0.3) + x.^(3-0.23) -2;
w=y.^(2.*x)-cos(y)+1;
q=sin(x)+3*x;
subplot(2,2,1)
plot(x,y,'-.y');
title('y = 2x^{0.3} + x^{3-0.23} - 2 ')
subplot(2,2,2)
plot(x,w,'--g')
title('w = y^{2x} - cos(y) + 1')
subplot(2,2,3)
plot(x,q,'>k');
title('q = sen(x) + 3x ')
subplot(2,2,4)
loglog(x,y,x,w,x,q)
title('y vs w vs q en escala logaritmica ')
xlabel('variable independiente')
ylabel('variables dependientes')
legend('y','w','q')