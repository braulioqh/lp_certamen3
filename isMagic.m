function respuesta=isMagic(M)
  %Se obtiene la cantidad de filas y columnas
  [nFilas nColumnas]=size(M);
  if(nFilas==nColumnas)
    %Se genera un vector del tamaño de filas y otro del tamaño
    %de las columnas, se iniciaran en cero y se utilizarán para 
    %sumar los elementos en cada columnas y cada fila
    sumaFilas=zeros(1,nFilas);
    sumaColumnas=zeros(1,nColumnas);
    %Se recorre la matriz para calcular el número de filas
    %y el número de columnas
    for i=1:nFilas
      for j=1:nColumnas
        sumaFilas(i)=sumaFilas(i)+M(i,j);
        sumaColumnas(j)=sumaColumnas(j)+M(i,j);
      end
    end
    
    respFila=true;
    for i=1:nFilas-1
      if sumaFilas(i)~=sumaFilas(i+1)
        respFila=false;
      end
    end
    respColumna=true;
    for i=1:nColumnas-1
      if sumaColumnas(i)~=sumaColumnas(i+1)
        respColumna=false;
      end
    end
    
    respuesta=respFila&&respColumna;
   else
    respuesta=false;
 end
      
    